const {createApp} = Vue;

createApp({
    data(){
        return{
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        }//Fechamento return
    },//Fechamento data 

    methods:{
        login(){
            // alert("Testando...");

            //Simulando uma requisão de login assincrona
            setTimeout(() => {
                if((this.usuario ==="Izadora" && this.senha ==="12345678") || 
                (this.usuario=== "iza" && this.senha === "123456")) {
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    // alert("Login efetuado com sucesso!");
                }//Fim do if
                else{
                    // alert("Usuario ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);

        }, //Fechamento login
    }//Fechamento methods


}).mount("#app");//Fechamento app