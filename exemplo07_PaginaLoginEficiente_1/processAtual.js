const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,

            erro:null,
            sucesso:null,

            usuario: "",
            senha:"",

        //Arrays (vetores) para armazenamento dos nomes de usuário e senhas
            usuarios:["admin", "Izadora", "Iza"],
            senhas: ["123", "321", "123"],
            userAdmin: false,
            mostrarEntrada: false,

            //Variáveis para tratamento das informações dos novos usuário
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            mostrarLista: false,

        };//Fechamento return
    },//Fechamento data

    methods:{
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                // this.valorDisplay = this.valorDisplay + numero.toString();

                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento getNumero

        Limpar(){
            this.valorDisplay = 0;
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento Limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                                break;
                            
                            case"-": 
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;

                    }//fim do switch
                    this.numeroAnterior = this.numeroAtual
                    this.numeroAtual = null;
                    this.operador = null;
                }//fim do if
                else{
                    this.numeroAnterior = displayAtual;

                }//fim do else
            }//fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            //this.valorDisplay = "0";                
        },//fim operações

        login(){
            // alert("Testando...");

            //Simulando uma requisão de login assincrona
            setTimeout(() => {
                if((this.usuario ==="Izadora" && this.senha ==="321") || 
                (this.usuario=== "Iza" && this.senha === "123")) {
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    window.location.href='calculadora'
                    // alert("Login efetuado com sucesso!");
                }//Fim do if
                else{
                    // alert("Usuario ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);

        }, //Fechamento login

        login2(){
            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;

                //Verificação de usuário e senha cadastrados ou não nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if(index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    //Registrando o usuário no LocalStorage para Lembrete de acessos
                    localStorage.setItem("usuario" , this.usuario);
                    localStorage.setItem("senha", this.senha);

                    //Verificando se o usuário é admin
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true; 
                        this.sucesso = "Logado como ADMIN!";
                    }

                }//Fechamento if
                else{
                    this.sucesso = null;
                    this.erro = "Usuário e / ou senha incorretos!";
                }      
            }, 1000);

        },//Fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "Carregando Página de Cadastro...";
                this.mostrarEntrada = true;

                //Espera estratégica antes do carregando da página
                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href = "cadastro.html";
                }, 1000);
            }//Fechamento if
            else{
                this.sucesso = null;
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.erro = "Sem privilégios ADMIN!!!";
                }, 1000);
            }//Fechamento else

        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.mostrarEntrada = false;

            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;

                if(this.usuario === "admin"){
                    /*Verificando de o novo usuário é diferente de vazio e se ele já não está cadastrando no sistema*/
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername !== " " && !this.newUsername.includes(".")){
                        //Validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword ===this.confirmPassword){
                            //Inserindo um novo usuário e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);

                            //Atualizando o usuário recém cadastrado no LocalStorage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));

                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso!";

                        }//Fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "Por favor, informe uma senha válida!!!";

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";                           
                        }//Fechamento else
                    }//Fechamento if includes
                    else{
                        this.erro = "Usuário inválido! Por favor digite um usuário diferente!";
                        this.sucesso = null;

                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                    }//Fechamento else

                }//Fechamento if admin
                else{
                    this.erro = "Não esta logado como ADMIN!";
                    this.sucesso = null;
                }//Fechamento else admin

            }, 1000);

        },//Fechamento adicionarUsuario

        listarUsuarios(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento listarUsuarios

        excluirUsuario(){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro = "O usuário ADMIN não pode ser excluido!!!";                    
                },500);
                return; //Força a saída deste bloco
            }//Fechamento do if
            
            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    
                    //Atualiza o array usuarios no Localstorage
                localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                localStorage.setItem("senhas", JSON.stringify(this.senhas));
                
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.erro = null;
                    this.sucesso = "Usuário excluido com sucesso!";
                }, 500
                );
                }//Fechamento if index
            }//Fechamento if
        },//Fechamento excluirUsuario

    },//Fechamento methods

}).mount("#app");//Fechamento app