const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
        };//Fechamento return
    },//Fechamento data

    methods:{
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                // this.valorDisplay = this.valorDisplay + numero.toString();

                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento getNumero

        Limpar(){
            this.valorDisplay = 0;
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento Limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                                break;
                            
                            case"-": 
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;

                    }//fim do switch
                    this.numeroAnterior = this.numeroAtual
                    this.numeroAtual = null;
                    this.operador = null;
                }//fim do if
                else{
                    this.numeroAnterior = displayAtual;

                }//fim do else
            }//fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
            //this.valorDisplay = "0";                
        },//fim operações

        login(){
            // alert("Testando...");

            //Simulando uma requisão de login assincrona
            setTimeout(() => {
                if((this.usuario ==="Izadora" && this.senha ==="12345678") || 
                (this.usuario=== "iza" && this.senha === "123456")) {
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    window.location.href='calculadora'
                    // alert("Login efetuado com sucesso!");
                }//Fim do if
                else{
                    // alert("Usuario ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);

        }, //Fechamento login
    }//Fechamento methods


}).mount("#app");//Fechamento app
    },//Fechamento methods

}).mount("#app");//Fechamento app